﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str">Motor,1;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LV.ExampleFinder" Type="Str">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;ExampleProgram&gt;
&lt;Title&gt;
	&lt;Text Locale="US"&gt;9503 Stepper Drive (Velocity Setpoint).lvproj&lt;/Text&gt;
&lt;/Title&gt;
&lt;Keywords&gt;
	&lt;Item&gt;9503&lt;/Item&gt;
	&lt;Item&gt;stepper&lt;/Item&gt;
	&lt;Item&gt;velocity&lt;/Item&gt;
	&lt;Item&gt;drive&lt;/Item&gt;
	&lt;Item&gt;motion&lt;/Item&gt;
	&lt;Item&gt;SoftMotion&lt;/Item&gt;
&lt;/Keywords&gt;
&lt;Navigation&gt;
	&lt;Item&gt;3410&lt;/Item&gt;
	&lt;Item&gt;3411&lt;/Item&gt;
	&lt;Item&gt;3412&lt;/Item&gt;
&lt;/Navigation&gt;
&lt;FileType&gt;LV Project&lt;/FileType&gt;
&lt;Metadata&gt;
&lt;Item Name="RTSupport"&gt;LV Project&lt;/Item&gt;
&lt;/Metadata&gt;
&lt;ProgrammingLanguages&gt;
&lt;Item&gt;LabVIEW&lt;/Item&gt;
&lt;/ProgrammingLanguages&gt;
&lt;RequiredSoftware&gt;
&lt;NiSoftware MinVersion="11.0"&gt;LabVIEW&lt;/NiSoftware&gt; 
&lt;/RequiredSoftware&gt;
&lt;RequiredMotionHardware&gt;
&lt;Device&gt;
&lt;Model&gt;751F&lt;/Model&gt;
&lt;/Device&gt;
&lt;/RequiredMotionHardware&gt;
&lt;/ExampleProgram&gt;</Property>
	<Property Name="NI.Project.Description" Type="Str">This example demonstrates how to use an NI 9503 C Series drive module in velocity mode. This is the simplest NI 9503 example and is suitable for applications that do slow velocity moves.

For applications requiring complex moves, coordinated moves, or SoftMotion axis integration, consider using one of the 9503 Stepper Drive (Position Profile).lvproj examples.

This example needs to be compiled for a specific FPGA target before use.

For information on moving this example to another FPGA target, refer to ni.com/info and enter info code fpgaex.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">true</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Support VIs" Type="Folder">
			<Item Name="Velocity Conversion StepsPerPeriod to RPS.vi" Type="VI" URL="../support/Velocity Conversion StepsPerPeriod to RPS.vi"/>
		</Item>
		<Item Name="GDP-3NI9503-Stepper-Drive-Host.vi" Type="VI" URL="../GDP-3NI9503-Stepper-Drive-Host.vi"/>
		<Item Name="GDP-NI9503-Stepper-Drive-Host.vi" Type="VI" URL="../GDP-NI9503-Stepper-Drive-Host.vi"/>
		<Item Name="NI 9503 Stepper Drive Velocity Setpoint.html" Type="Document" URL="../documentation/NI 9503 Stepper Drive Velocity Setpoint.html"/>
		<Item Name="stepping-motor-characteristic data.txt" Type="Document" URL="../documentation/stepping-motor-characteristic data.txt"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				<Item Name="NI_SoftMotion_MotorControlIP.lvlib" Type="Library" URL="/&lt;vilib&gt;/Motion/MotorControl/NI_SoftMotion_MotorControlIP.lvlib"/>
			</Item>
			<Item Name="GDP-9503-Support.lvlib" Type="Library" URL="../GDP-9503-Support.lvlib"/>
			<Item Name="GDP-9503.lvlib" Type="Library" URL="../GDP-9503.lvlib"/>
			<Item Name="GDP-NI9503-Stepp_FPGATarget_Main_DoDqZffLYJo.lvbitx" Type="Document" URL="../FPGA Bitfiles/GDP-NI9503-Stepp_FPGATarget_Main_DoDqZffLYJo.lvbitx"/>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="RT CompactRIO Target" Type="RT CompactRIO">
		<Property Name="alias.name" Type="Str">RT CompactRIO Target</Property>
		<Property Name="alias.value" Type="Str">dldev010</Property>
		<Property Name="CCSymbols" Type="Str">TARGET_TYPE,RT;OS,Linux;CPU,ARM;DeviceCode,76D6;</Property>
		<Property Name="crio.ControllerPID" Type="Str">76D6</Property>
		<Property Name="crio.family" Type="Str">ARMLinux</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">8</Property>
		<Property Name="host.TargetOSID" Type="UInt">8</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">false</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/home/lvuser/natinst/bin</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str">Listen 8000

NI.ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
DirectoryIndex index.htm
WorkerLimit 10
InactivityTimeout 60

LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule

#
# Pipeline Definition
#

SetConnector netConnector

AddHandler LVAuth
AddHandler LVRFP

AddHandler fileHandler ""

AddOutputFilter chunkFilter


</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="Chassis" Type="cRIO Chassis">
			<Property Name="crio.ProgrammingMode" Type="Str">fpga</Property>
			<Property Name="crio.ResourceID" Type="Str">RIO0</Property>
			<Property Name="crio.Type" Type="Str">cRIO-9068</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="FPGA Target" Type="FPGA Target">
				<Property Name="AutoRun" Type="Bool">false</Property>
				<Property Name="configString.guid" Type="Str">{006F05D7-61BB-407A-979B-D192B71E1E34}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Acceleration;WriteArb=1"{010E016C-E1E4-44AA-AF12-A394C6BFD24D}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Acceleration;WriteArb=1"{026FDC5C-D601-4357-9DCA-D02573CA54BC}resource=/crio_AO/AO2;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{07251ABD-BBB2-4D32-BE9F-144658305956}resource=/crio_AO/AO1;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{0F53BB40-C28C-45BC-B323-1DFDF3BB500A}resource=/crio_DIO/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{12C364F6-6FC3-43ED-A15E-1C831422CA44}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9503[crioConfig.End]{13CD6CAA-198B-4FD9-AE0A-F4D520AB051E}resource=/crio_DIO/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{14DD4532-44FC-4FE7-AEE7-92449120EEA8}resource=/crio_DIO/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{15233ADB-26D7-4AB8-9A49-F70EC69EE51D}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Fault;WriteArb=1"{1901449E-2C86-49CB-9C06-17F5B0B1D0B0}resource=/crio_DIO/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{201594B3-DA05-472F-B454-E93CB39D9CD2}resource=/crio_M1/Phase A Current;0;ReadMethodType=I16{21E02E54-3F81-4804-968A-C4D015E2820D}resource=/crio_DIO/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{2224891D-A82A-4BCD-AEF6-8F587802C8FC}resource=/crio_DIO/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{277EBE1A-A6CC-4EDF-A83E-FCB2C8061743}resource=/crio_M3/Phase A Current;0;ReadMethodType=I16{2A83F047-5CEA-44D6-93FB-244FB6A9A93F}resource=/crio_M3/Phase A Pos;0;WriteMethodType=bool{2BDA572E-33F4-4691-AA99-80CBF303AAE1}resource=/crio_DIO/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{2BF18089-F30E-49F3-B51F-A4922E402A1F}resource=/crio_DIO/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{2E3868E1-2357-4D16-9E98-C531AEED6315}resource=/crio_RTD/RTD3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{3122BB88-70C9-4ECE-87BE-FCAE9D40D781}resource=/crio_DIO/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{37AC7FE7-D68D-4BB6-8B89-B149D0321D6D}resource=/crio_DIO/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{38D49020-B080-4144-B3E3-E4A0A4306257}resource=/crio_DIO/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{3BD16E5F-9BC1-4A1E-AC09-2971644802DB}resource=/crio_AO/AO3;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{3C46C8E7-A00A-4453-A520-F51672F00837}resource=/crio_DIO/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{3C642664-A6F3-4C66-A1B9-0213307023E5}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9215,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{3D2524F3-2686-4566-8032-1641526799AB}resource=/crio_M1/Phase A Neg;0;WriteMethodType=bool{3D3E5081-5FBE-4379-8912-E9002F32C7B9}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Velocity;WriteArb=1"{3E530D7E-864F-43D0-AF49-15B5CDE2BB5F}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{40BE5CC7-CDD4-420F-AA25-7F1DA696E32B}resource=/crio_DIO/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{4477D065-8069-4CA4-80E6-F0A79D39BF55}resource=/crio_DIO/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{45FD5D6A-17EA-48FB-B0AA-51845FEDA88B}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Fault;WriteArb=1"{46106632-8333-4468-B006-E2D67DCC25E9}resource=/crio_M2/Vsup Voltage;0;ReadMethodType=u16{47D82495-122D-47CE-9129-3E94D4F674B7}resource=/crio_DIO/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{47F67B21-77C9-4082-B039-4D98371FBAB8}resource=/crio_M2/Phase B Current;0;ReadMethodType=I16{48C0C5DB-0FCD-4D2A-B858-B8FA9CF450CD}resource=/crio_DIO/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{49E5CD12-D57E-4B7A-9D48-85E14B4FA6D5}resource=/crio_M2/Phase B Pos;0;WriteMethodType=bool{4EC7DC1B-D472-4B38-B84D-723A1938F8B8}resource=/crio_M3/User LED;0;WriteMethodType=bool{4F3F9BF8-D79D-45BD-894A-4624B836C30A}resource=/crio_DIO/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{4F6DC4BE-4506-4BEF-8065-C5F46E77ECA9}resource=/crio_DIO/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{51429A00-FAAF-46EF-9962-60AAD867809A}resource=/crio_M3/Phase B Current;0;ReadMethodType=I16{51B91DB3-8528-4B79-A63D-C6FB8DF2F999}resource=/crio_M2/Phase A Neg;0;WriteMethodType=bool{53F32106-4D81-471E-9300-07B9D924A859}resource=/Scan Clock;0;ReadMethodType=bool{54AFFFEC-1B0D-4C24-B6AC-D10F1418A425}resource=/crio_AI/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{5A5BC916-492C-4E93-890E-945655CDB66D}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{63AE5859-9011-477E-8E4F-375473740DAF}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{63B3BFC5-043E-4D3C-8292-709646D94218}resource=/crio_DIO/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{67D6B436-478D-42AD-A98A-02D9F02511E0}resource=/crio_AO/AO0;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{6ADFC21D-443B-4DBA-B07E-8CC9C92D5007}resource=/crio_DIO/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{6BEF824D-AA88-48B6-8919-AC5612FD709C}resource=/crio_M2/Phase A Current;0;ReadMethodType=I16{6E015061-A9DB-4F6A-BAB5-D7142A92B1EC}resource=/crio_DIO/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{70366361-0FD2-486E-A521-CB6B726DA0B2}resource=/crio_RTD/RTD0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{725EBB60-DE48-4E19-B249-4727EFC6771D}resource=/crio_DIO/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{72C93886-6840-4846-AB65-9493DE3510AF}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Status;WriteArb=1"{74591987-52CC-4938-8CA3-61B3402D61D6}resource=/crio_DIO/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{74B52144-4566-4772-9E96-53028B4CE236}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{7B3B7C2C-A1BD-4B0E-AE2B-C895184A8CB1}resource=/crio_DIO/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{7BA7AB73-6BDD-4EE1-B207-ADB98264EBA9}resource=/crio_M3/Phase A Neg;0;WriteMethodType=bool{7D1AE7CD-0818-48B0-975B-F4F914EF5AB4}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_ERC;WriteArb=1"{7E1AD0C5-A11A-4F87-859D-F33EFEE09DE8}resource=/crio_DIO/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{82E44D0E-A81F-4A61-B400-236726FE216E}resource=/crio_DIO/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{8458B58A-7AD1-4E16-BAC5-EBE2DF32D274}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_ERC;WriteArb=1"{85EB5E73-E7F0-475C-BC32-B2711096EAE3}resource=/crio_AI/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{85F0BA7B-C7CC-49B0-81A1-495F09F5E23B}resource=/crio_RTD/RTD1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{880D9FFC-FD5D-4A1F-99F4-93F82E1B2CD6}resource=/crio_DIO/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{89784F34-D11A-4B88-9628-F1F96A36103A}resource=/crio_M2/Phase B Neg;0;WriteMethodType=bool{8B27681D-ED0E-4840-BBF9-195F6E85BEF9}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9269,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.HotSwapMode=0,cRIOModule.RsiAttributes=[crioConfig.End]{8E931E92-530F-4D82-A5D5-8D08593EA76F}resource=/crio_M2/User LED;0;WriteMethodType=bool{9479962B-5A8F-432A-8D41-7D25190ED3D8}resource=/crio_DIO/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{96719503-0785-4051-BC5B-E5DE7BDEE8D6}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Fault;WriteArb=1"{96F5C267-2D92-4F2D-ADF6-B98333008BCD}resource=/crio_DIO/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{98B58B44-DAD4-42D3-AFD4-3696CECDBDA5}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9503[crioConfig.End]{9DA713EE-5A9C-44B1-9400-77B8EDA367DD}resource=/crio_M1/Phase A Pos;0;WriteMethodType=bool{A1A2D640-42E7-4F71-8570-D754AAAD4E54}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Velocity;WriteArb=1"{A3E30E0A-2957-4464-9D86-6E0D826420BA}resource=/crio_DIO/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{A58590BE-D062-4BCD-A7A3-C2C956BA19CF}resource=/crio_RTD/RTD2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{A62615B2-7518-44A2-9FE1-8418CB3DF33C}resource=/Chassis Temperature;0;ReadMethodType=i16{BA0759DE-DEB9-4115-8F06-4C2D0579BA83}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9503[crioConfig.End]{BAB8EF2B-F6C8-4B14-98FF-6CD2AF45823E}resource=/crio_M3/Phase B Pos;0;WriteMethodType=bool{BC363C0F-8C6E-42DC-84FA-8C6E274FEFD1}resource=/crio_DIO/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{BF8083E2-842C-4743-87AC-39EA4178D766}resource=/crio_M1/Vsup Voltage;0;ReadMethodType=u16{C08DF885-E8A2-4D7A-8B48-F46326296823}resource=/USER FPGA LED;0;ReadMethodType=u8;WriteMethodType=u8{C1927FA7-D3DC-4FF9-B56B-E922DD08C0B4}resource=/crio_DIO/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{C351C620-4AA5-4984-9222-41FDFE5DD228}resource=/crio_M3/Vsup Voltage;0;ReadMethodType=u16{C54EF70B-9469-4FB7-978D-65971D7E30D1}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9217,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.RTD_A=3,908300E-3,cRIOModule.AI0.RTD_B=-5,775000E-7,cRIOModule.AI0.RTD_C=-4,183000E-12,cRIOModule.AI0.RTD_Ro=1,000000E+2,cRIOModule.AI0.RTDType=1,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.RTD_A=3,908300E-3,cRIOModule.AI1.RTD_B=-5,775000E-7,cRIOModule.AI1.RTD_C=-4,183000E-12,cRIOModule.AI1.RTD_Ro=1,000000E+2,cRIOModule.AI1.RTDType=1,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.RTD_A=3,908300E-3,cRIOModule.AI2.RTD_B=-5,775000E-7,cRIOModule.AI2.RTD_C=-4,183000E-12,cRIOModule.AI2.RTD_Ro=1,000000E+2,cRIOModule.AI2.RTDType=1,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.RTD_A=3,908300E-3,cRIOModule.AI3.RTD_B=-5,775000E-7,cRIOModule.AI3.RTD_C=-4,183000E-12,cRIOModule.AI3.RTD_Ro=1,000000E+2,cRIOModule.AI3.RTDType=1,cRIOModule.Conversion Time=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C6E5A006-DD94-4689-87E8-BE6797E60D0E}resource=/crio_M1/Phase B Pos;0;WriteMethodType=bool{CC2385AB-B3FF-4851-92E2-8DDCA015C218}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Acceleration;WriteArb=1"{CE99DF16-01C5-4A2B-9DF9-F78C5E3A7A60}resource=/crio_M2/Phase A Pos;0;WriteMethodType=bool{CF3E83A5-BCDB-4F41-A0C0-5429AD8314C6}resource=/crio_DIO/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{D2112507-8C9A-4C91-9924-650FA96DD702}resource=/crio_M1/Phase B Current;0;ReadMethodType=I16{D6EA9CCA-640E-4584-B32B-4986F6D93E94}resource=/crio_DIO/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{D8F98CFB-0029-4C76-BDA7-72A9D634104D}resource=/crio_M3/Phase B Neg;0;WriteMethodType=bool{E0B41708-B684-4017-B93F-1D76B222FA17}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Status;WriteArb=1"{E1099124-0820-457D-B873-1105BA1A109F}resource=/crio_DIO/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{E6B79732-A2E6-48BD-9785-AF8DD5BB77E0}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Status;WriteArb=1"{E851EB27-3902-4779-8A5E-3E37CD923599}resource=/crio_AI/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{E96C53ED-AA73-4E47-974B-DD37928C487D}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_ERC;WriteArb=1"{ECA728EE-42D4-4F79-B429-F266DA77039E}resource=/crio_AI/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{F0EF8321-578F-4BB4-B502-EC1F74FEEA29}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Velocity;WriteArb=1"{F1EC0C53-F395-4B6B-8830-D525ED9D26D0}resource=/crio_M1/User LED;0;WriteMethodType=bool{F5D92A55-D78C-49F3-9C7F-EA1BC351B7D6}resource=/crio_DIO/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{F60E166B-0FAA-4B74-A8B8-BB5499263F2D}resource=/crio_DIO/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{FA13C610-4BD7-4402-AE9C-56D700D751F4}resource=/crio_M1/Phase B Neg;0;WriteMethodType=boolcRIO-9068/Motor,1;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9068FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EAI/AI0resource=/crio_AI/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI/AI1resource=/crio_AI/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI/AI2resource=/crio_AI/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI/AI3resource=/crio_AI/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9215,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]AO/AO0resource=/crio_AO/AO0;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO/AO1resource=/crio_AO/AO1;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO/AO2resource=/crio_AO/AO2;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO/AO3resource=/crio_AO/AO3;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9269,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.HotSwapMode=0,cRIOModule.RsiAttributes=[crioConfig.End]Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9068/Motor,1;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9068FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]DIO/DIO0resource=/crio_DIO/DIO0;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO10resource=/crio_DIO/DIO10;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO11resource=/crio_DIO/DIO11;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO12resource=/crio_DIO/DIO12;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO13resource=/crio_DIO/DIO13;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO14resource=/crio_DIO/DIO14;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO15:8resource=/crio_DIO/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO15resource=/crio_DIO/DIO15;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO16resource=/crio_DIO/DIO16;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO17resource=/crio_DIO/DIO17;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO18resource=/crio_DIO/DIO18;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO19resource=/crio_DIO/DIO19;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO1resource=/crio_DIO/DIO1;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO20resource=/crio_DIO/DIO20;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO21resource=/crio_DIO/DIO21;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO22resource=/crio_DIO/DIO22;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO23:16resource=/crio_DIO/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO23resource=/crio_DIO/DIO23;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO24resource=/crio_DIO/DIO24;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO25resource=/crio_DIO/DIO25;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO26resource=/crio_DIO/DIO26;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO27resource=/crio_DIO/DIO27;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO28resource=/crio_DIO/DIO28;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO29resource=/crio_DIO/DIO29;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO2resource=/crio_DIO/DIO2;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO30resource=/crio_DIO/DIO30;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO31:0resource=/crio_DIO/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32DIO/DIO31:24resource=/crio_DIO/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO31resource=/crio_DIO/DIO31;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO3resource=/crio_DIO/DIO3;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO4resource=/crio_DIO/DIO4;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO5resource=/crio_DIO/DIO5;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO6resource=/crio_DIO/DIO6;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO7:0resource=/crio_DIO/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO7resource=/crio_DIO/DIO7;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO8resource=/crio_DIO/DIO8;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO9resource=/crio_DIO/DIO9;0;ReadMethodType=bool;WriteMethodType=boolDIO[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]M1/Phase A Currentresource=/crio_M1/Phase A Current;0;ReadMethodType=I16M1/Phase A Negresource=/crio_M1/Phase A Neg;0;WriteMethodType=boolM1/Phase A Posresource=/crio_M1/Phase A Pos;0;WriteMethodType=boolM1/Phase B Currentresource=/crio_M1/Phase B Current;0;ReadMethodType=I16M1/Phase B Negresource=/crio_M1/Phase B Neg;0;WriteMethodType=boolM1/Phase B Posresource=/crio_M1/Phase B Pos;0;WriteMethodType=boolM1/User LEDresource=/crio_M1/User LED;0;WriteMethodType=boolM1/Vsup Voltageresource=/crio_M1/Vsup Voltage;0;ReadMethodType=u16M1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9503[crioConfig.End]M1_Acceleration"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Acceleration;WriteArb=1"M1_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_ERC;WriteArb=1"M1_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Fault;WriteArb=1"M1_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Status;WriteArb=1"M1_Velocity"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Velocity;WriteArb=1"M2/Phase A Currentresource=/crio_M2/Phase A Current;0;ReadMethodType=I16M2/Phase A Negresource=/crio_M2/Phase A Neg;0;WriteMethodType=boolM2/Phase A Posresource=/crio_M2/Phase A Pos;0;WriteMethodType=boolM2/Phase B Currentresource=/crio_M2/Phase B Current;0;ReadMethodType=I16M2/Phase B Negresource=/crio_M2/Phase B Neg;0;WriteMethodType=boolM2/Phase B Posresource=/crio_M2/Phase B Pos;0;WriteMethodType=boolM2/User LEDresource=/crio_M2/User LED;0;WriteMethodType=boolM2/Vsup Voltageresource=/crio_M2/Vsup Voltage;0;ReadMethodType=u16M2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9503[crioConfig.End]M2_Acceleration"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Acceleration;WriteArb=1"M2_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_ERC;WriteArb=1"M2_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Fault;WriteArb=1"M2_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Status;WriteArb=1"M2_Velocity"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Velocity;WriteArb=1"M3/Phase A Currentresource=/crio_M3/Phase A Current;0;ReadMethodType=I16M3/Phase A Negresource=/crio_M3/Phase A Neg;0;WriteMethodType=boolM3/Phase A Posresource=/crio_M3/Phase A Pos;0;WriteMethodType=boolM3/Phase B Currentresource=/crio_M3/Phase B Current;0;ReadMethodType=I16M3/Phase B Negresource=/crio_M3/Phase B Neg;0;WriteMethodType=boolM3/Phase B Posresource=/crio_M3/Phase B Pos;0;WriteMethodType=boolM3/User LEDresource=/crio_M3/User LED;0;WriteMethodType=boolM3/Vsup Voltageresource=/crio_M3/Vsup Voltage;0;ReadMethodType=u16M3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9503[crioConfig.End]M3_Acceleration"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Acceleration;WriteArb=1"M3_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_ERC;WriteArb=1"M3_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Fault;WriteArb=1"M3_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Status;WriteArb=1"M3_Velocity"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Velocity;WriteArb=1"RTD/RTD0resource=/crio_RTD/RTD0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD/RTD1resource=/crio_RTD/RTD1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD/RTD2resource=/crio_RTD/RTD2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD/RTD3resource=/crio_RTD/RTD3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9217,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.RTD_A=3,908300E-3,cRIOModule.AI0.RTD_B=-5,775000E-7,cRIOModule.AI0.RTD_C=-4,183000E-12,cRIOModule.AI0.RTD_Ro=1,000000E+2,cRIOModule.AI0.RTDType=1,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.RTD_A=3,908300E-3,cRIOModule.AI1.RTD_B=-5,775000E-7,cRIOModule.AI1.RTD_C=-4,183000E-12,cRIOModule.AI1.RTD_Ro=1,000000E+2,cRIOModule.AI1.RTDType=1,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.RTD_A=3,908300E-3,cRIOModule.AI2.RTD_B=-5,775000E-7,cRIOModule.AI2.RTD_C=-4,183000E-12,cRIOModule.AI2.RTD_Ro=1,000000E+2,cRIOModule.AI2.RTDType=1,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.RTD_A=3,908300E-3,cRIOModule.AI3.RTD_B=-5,775000E-7,cRIOModule.AI3.RTD_C=-4,183000E-12,cRIOModule.AI3.RTD_Ro=1,000000E+2,cRIOModule.AI3.RTDType=1,cRIOModule.Conversion Time=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolUSER FPGA LEDresource=/USER FPGA LED;0;ReadMethodType=u8;WriteMethodType=u8</Property>
				<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">cRIO-9068/Motor,1;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9068FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Property Name="Resource Name" Type="Str">RIO0</Property>
				<Property Name="Target Class" Type="Str">cRIO-9068</Property>
				<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
				<Item Name="Support VIs" Type="Folder">
					<Item Name="PI Current reetrant.vi" Type="VI" URL="../support/PI Current reetrant.vi">
						<Property Name="configString.guid" Type="Str">{006F05D7-61BB-407A-979B-D192B71E1E34}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Acceleration;WriteArb=1"{010E016C-E1E4-44AA-AF12-A394C6BFD24D}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Acceleration;WriteArb=1"{026FDC5C-D601-4357-9DCA-D02573CA54BC}resource=/crio_AO/AO2;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{07251ABD-BBB2-4D32-BE9F-144658305956}resource=/crio_AO/AO1;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{0F53BB40-C28C-45BC-B323-1DFDF3BB500A}resource=/crio_DIO/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{12C364F6-6FC3-43ED-A15E-1C831422CA44}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9503[crioConfig.End]{13CD6CAA-198B-4FD9-AE0A-F4D520AB051E}resource=/crio_DIO/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{14DD4532-44FC-4FE7-AEE7-92449120EEA8}resource=/crio_DIO/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{15233ADB-26D7-4AB8-9A49-F70EC69EE51D}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Fault;WriteArb=1"{1901449E-2C86-49CB-9C06-17F5B0B1D0B0}resource=/crio_DIO/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{201594B3-DA05-472F-B454-E93CB39D9CD2}resource=/crio_M1/Phase A Current;0;ReadMethodType=I16{21E02E54-3F81-4804-968A-C4D015E2820D}resource=/crio_DIO/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{2224891D-A82A-4BCD-AEF6-8F587802C8FC}resource=/crio_DIO/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{277EBE1A-A6CC-4EDF-A83E-FCB2C8061743}resource=/crio_M3/Phase A Current;0;ReadMethodType=I16{2A83F047-5CEA-44D6-93FB-244FB6A9A93F}resource=/crio_M3/Phase A Pos;0;WriteMethodType=bool{2BDA572E-33F4-4691-AA99-80CBF303AAE1}resource=/crio_DIO/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{2BF18089-F30E-49F3-B51F-A4922E402A1F}resource=/crio_DIO/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{2E3868E1-2357-4D16-9E98-C531AEED6315}resource=/crio_RTD/RTD3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{3122BB88-70C9-4ECE-87BE-FCAE9D40D781}resource=/crio_DIO/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{37AC7FE7-D68D-4BB6-8B89-B149D0321D6D}resource=/crio_DIO/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{38D49020-B080-4144-B3E3-E4A0A4306257}resource=/crio_DIO/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{3BD16E5F-9BC1-4A1E-AC09-2971644802DB}resource=/crio_AO/AO3;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{3C46C8E7-A00A-4453-A520-F51672F00837}resource=/crio_DIO/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{3C642664-A6F3-4C66-A1B9-0213307023E5}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9215,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{3D2524F3-2686-4566-8032-1641526799AB}resource=/crio_M1/Phase A Neg;0;WriteMethodType=bool{3D3E5081-5FBE-4379-8912-E9002F32C7B9}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Velocity;WriteArb=1"{3E530D7E-864F-43D0-AF49-15B5CDE2BB5F}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{40BE5CC7-CDD4-420F-AA25-7F1DA696E32B}resource=/crio_DIO/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{4477D065-8069-4CA4-80E6-F0A79D39BF55}resource=/crio_DIO/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{45FD5D6A-17EA-48FB-B0AA-51845FEDA88B}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Fault;WriteArb=1"{46106632-8333-4468-B006-E2D67DCC25E9}resource=/crio_M2/Vsup Voltage;0;ReadMethodType=u16{47D82495-122D-47CE-9129-3E94D4F674B7}resource=/crio_DIO/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{47F67B21-77C9-4082-B039-4D98371FBAB8}resource=/crio_M2/Phase B Current;0;ReadMethodType=I16{48C0C5DB-0FCD-4D2A-B858-B8FA9CF450CD}resource=/crio_DIO/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{49E5CD12-D57E-4B7A-9D48-85E14B4FA6D5}resource=/crio_M2/Phase B Pos;0;WriteMethodType=bool{4EC7DC1B-D472-4B38-B84D-723A1938F8B8}resource=/crio_M3/User LED;0;WriteMethodType=bool{4F3F9BF8-D79D-45BD-894A-4624B836C30A}resource=/crio_DIO/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{4F6DC4BE-4506-4BEF-8065-C5F46E77ECA9}resource=/crio_DIO/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{51429A00-FAAF-46EF-9962-60AAD867809A}resource=/crio_M3/Phase B Current;0;ReadMethodType=I16{51B91DB3-8528-4B79-A63D-C6FB8DF2F999}resource=/crio_M2/Phase A Neg;0;WriteMethodType=bool{53F32106-4D81-471E-9300-07B9D924A859}resource=/Scan Clock;0;ReadMethodType=bool{54AFFFEC-1B0D-4C24-B6AC-D10F1418A425}resource=/crio_AI/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{5A5BC916-492C-4E93-890E-945655CDB66D}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{63AE5859-9011-477E-8E4F-375473740DAF}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{63B3BFC5-043E-4D3C-8292-709646D94218}resource=/crio_DIO/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{67D6B436-478D-42AD-A98A-02D9F02511E0}resource=/crio_AO/AO0;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{6ADFC21D-443B-4DBA-B07E-8CC9C92D5007}resource=/crio_DIO/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{6BEF824D-AA88-48B6-8919-AC5612FD709C}resource=/crio_M2/Phase A Current;0;ReadMethodType=I16{6E015061-A9DB-4F6A-BAB5-D7142A92B1EC}resource=/crio_DIO/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{70366361-0FD2-486E-A521-CB6B726DA0B2}resource=/crio_RTD/RTD0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{725EBB60-DE48-4E19-B249-4727EFC6771D}resource=/crio_DIO/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{72C93886-6840-4846-AB65-9493DE3510AF}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Status;WriteArb=1"{74591987-52CC-4938-8CA3-61B3402D61D6}resource=/crio_DIO/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{74B52144-4566-4772-9E96-53028B4CE236}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{7B3B7C2C-A1BD-4B0E-AE2B-C895184A8CB1}resource=/crio_DIO/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{7BA7AB73-6BDD-4EE1-B207-ADB98264EBA9}resource=/crio_M3/Phase A Neg;0;WriteMethodType=bool{7D1AE7CD-0818-48B0-975B-F4F914EF5AB4}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_ERC;WriteArb=1"{7E1AD0C5-A11A-4F87-859D-F33EFEE09DE8}resource=/crio_DIO/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{82E44D0E-A81F-4A61-B400-236726FE216E}resource=/crio_DIO/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{8458B58A-7AD1-4E16-BAC5-EBE2DF32D274}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_ERC;WriteArb=1"{85EB5E73-E7F0-475C-BC32-B2711096EAE3}resource=/crio_AI/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{85F0BA7B-C7CC-49B0-81A1-495F09F5E23B}resource=/crio_RTD/RTD1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{880D9FFC-FD5D-4A1F-99F4-93F82E1B2CD6}resource=/crio_DIO/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{89784F34-D11A-4B88-9628-F1F96A36103A}resource=/crio_M2/Phase B Neg;0;WriteMethodType=bool{8B27681D-ED0E-4840-BBF9-195F6E85BEF9}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9269,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.HotSwapMode=0,cRIOModule.RsiAttributes=[crioConfig.End]{8E931E92-530F-4D82-A5D5-8D08593EA76F}resource=/crio_M2/User LED;0;WriteMethodType=bool{9479962B-5A8F-432A-8D41-7D25190ED3D8}resource=/crio_DIO/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{96719503-0785-4051-BC5B-E5DE7BDEE8D6}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Fault;WriteArb=1"{96F5C267-2D92-4F2D-ADF6-B98333008BCD}resource=/crio_DIO/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{98B58B44-DAD4-42D3-AFD4-3696CECDBDA5}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9503[crioConfig.End]{9DA713EE-5A9C-44B1-9400-77B8EDA367DD}resource=/crio_M1/Phase A Pos;0;WriteMethodType=bool{A1A2D640-42E7-4F71-8570-D754AAAD4E54}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Velocity;WriteArb=1"{A3E30E0A-2957-4464-9D86-6E0D826420BA}resource=/crio_DIO/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{A58590BE-D062-4BCD-A7A3-C2C956BA19CF}resource=/crio_RTD/RTD2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{A62615B2-7518-44A2-9FE1-8418CB3DF33C}resource=/Chassis Temperature;0;ReadMethodType=i16{BA0759DE-DEB9-4115-8F06-4C2D0579BA83}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9503[crioConfig.End]{BAB8EF2B-F6C8-4B14-98FF-6CD2AF45823E}resource=/crio_M3/Phase B Pos;0;WriteMethodType=bool{BC363C0F-8C6E-42DC-84FA-8C6E274FEFD1}resource=/crio_DIO/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{BF8083E2-842C-4743-87AC-39EA4178D766}resource=/crio_M1/Vsup Voltage;0;ReadMethodType=u16{C08DF885-E8A2-4D7A-8B48-F46326296823}resource=/USER FPGA LED;0;ReadMethodType=u8;WriteMethodType=u8{C1927FA7-D3DC-4FF9-B56B-E922DD08C0B4}resource=/crio_DIO/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{C351C620-4AA5-4984-9222-41FDFE5DD228}resource=/crio_M3/Vsup Voltage;0;ReadMethodType=u16{C54EF70B-9469-4FB7-978D-65971D7E30D1}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9217,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.RTD_A=3,908300E-3,cRIOModule.AI0.RTD_B=-5,775000E-7,cRIOModule.AI0.RTD_C=-4,183000E-12,cRIOModule.AI0.RTD_Ro=1,000000E+2,cRIOModule.AI0.RTDType=1,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.RTD_A=3,908300E-3,cRIOModule.AI1.RTD_B=-5,775000E-7,cRIOModule.AI1.RTD_C=-4,183000E-12,cRIOModule.AI1.RTD_Ro=1,000000E+2,cRIOModule.AI1.RTDType=1,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.RTD_A=3,908300E-3,cRIOModule.AI2.RTD_B=-5,775000E-7,cRIOModule.AI2.RTD_C=-4,183000E-12,cRIOModule.AI2.RTD_Ro=1,000000E+2,cRIOModule.AI2.RTDType=1,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.RTD_A=3,908300E-3,cRIOModule.AI3.RTD_B=-5,775000E-7,cRIOModule.AI3.RTD_C=-4,183000E-12,cRIOModule.AI3.RTD_Ro=1,000000E+2,cRIOModule.AI3.RTDType=1,cRIOModule.Conversion Time=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C6E5A006-DD94-4689-87E8-BE6797E60D0E}resource=/crio_M1/Phase B Pos;0;WriteMethodType=bool{CC2385AB-B3FF-4851-92E2-8DDCA015C218}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Acceleration;WriteArb=1"{CE99DF16-01C5-4A2B-9DF9-F78C5E3A7A60}resource=/crio_M2/Phase A Pos;0;WriteMethodType=bool{CF3E83A5-BCDB-4F41-A0C0-5429AD8314C6}resource=/crio_DIO/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{D2112507-8C9A-4C91-9924-650FA96DD702}resource=/crio_M1/Phase B Current;0;ReadMethodType=I16{D6EA9CCA-640E-4584-B32B-4986F6D93E94}resource=/crio_DIO/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{D8F98CFB-0029-4C76-BDA7-72A9D634104D}resource=/crio_M3/Phase B Neg;0;WriteMethodType=bool{E0B41708-B684-4017-B93F-1D76B222FA17}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Status;WriteArb=1"{E1099124-0820-457D-B873-1105BA1A109F}resource=/crio_DIO/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{E6B79732-A2E6-48BD-9785-AF8DD5BB77E0}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Status;WriteArb=1"{E851EB27-3902-4779-8A5E-3E37CD923599}resource=/crio_AI/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{E96C53ED-AA73-4E47-974B-DD37928C487D}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_ERC;WriteArb=1"{ECA728EE-42D4-4F79-B429-F266DA77039E}resource=/crio_AI/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{F0EF8321-578F-4BB4-B502-EC1F74FEEA29}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Velocity;WriteArb=1"{F1EC0C53-F395-4B6B-8830-D525ED9D26D0}resource=/crio_M1/User LED;0;WriteMethodType=bool{F5D92A55-D78C-49F3-9C7F-EA1BC351B7D6}resource=/crio_DIO/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{F60E166B-0FAA-4B74-A8B8-BB5499263F2D}resource=/crio_DIO/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{FA13C610-4BD7-4402-AE9C-56D700D751F4}resource=/crio_M1/Phase B Neg;0;WriteMethodType=boolcRIO-9068/Motor,1;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9068FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
						<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EAI/AI0resource=/crio_AI/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI/AI1resource=/crio_AI/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI/AI2resource=/crio_AI/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI/AI3resource=/crio_AI/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9215,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]AO/AO0resource=/crio_AO/AO0;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO/AO1resource=/crio_AO/AO1;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO/AO2resource=/crio_AO/AO2;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO/AO3resource=/crio_AO/AO3;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9269,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.HotSwapMode=0,cRIOModule.RsiAttributes=[crioConfig.End]Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9068/Motor,1;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9068FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]DIO/DIO0resource=/crio_DIO/DIO0;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO10resource=/crio_DIO/DIO10;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO11resource=/crio_DIO/DIO11;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO12resource=/crio_DIO/DIO12;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO13resource=/crio_DIO/DIO13;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO14resource=/crio_DIO/DIO14;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO15:8resource=/crio_DIO/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO15resource=/crio_DIO/DIO15;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO16resource=/crio_DIO/DIO16;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO17resource=/crio_DIO/DIO17;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO18resource=/crio_DIO/DIO18;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO19resource=/crio_DIO/DIO19;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO1resource=/crio_DIO/DIO1;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO20resource=/crio_DIO/DIO20;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO21resource=/crio_DIO/DIO21;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO22resource=/crio_DIO/DIO22;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO23:16resource=/crio_DIO/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO23resource=/crio_DIO/DIO23;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO24resource=/crio_DIO/DIO24;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO25resource=/crio_DIO/DIO25;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO26resource=/crio_DIO/DIO26;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO27resource=/crio_DIO/DIO27;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO28resource=/crio_DIO/DIO28;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO29resource=/crio_DIO/DIO29;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO2resource=/crio_DIO/DIO2;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO30resource=/crio_DIO/DIO30;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO31:0resource=/crio_DIO/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32DIO/DIO31:24resource=/crio_DIO/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO31resource=/crio_DIO/DIO31;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO3resource=/crio_DIO/DIO3;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO4resource=/crio_DIO/DIO4;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO5resource=/crio_DIO/DIO5;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO6resource=/crio_DIO/DIO6;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO7:0resource=/crio_DIO/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO7resource=/crio_DIO/DIO7;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO8resource=/crio_DIO/DIO8;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO9resource=/crio_DIO/DIO9;0;ReadMethodType=bool;WriteMethodType=boolDIO[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]M1/Phase A Currentresource=/crio_M1/Phase A Current;0;ReadMethodType=I16M1/Phase A Negresource=/crio_M1/Phase A Neg;0;WriteMethodType=boolM1/Phase A Posresource=/crio_M1/Phase A Pos;0;WriteMethodType=boolM1/Phase B Currentresource=/crio_M1/Phase B Current;0;ReadMethodType=I16M1/Phase B Negresource=/crio_M1/Phase B Neg;0;WriteMethodType=boolM1/Phase B Posresource=/crio_M1/Phase B Pos;0;WriteMethodType=boolM1/User LEDresource=/crio_M1/User LED;0;WriteMethodType=boolM1/Vsup Voltageresource=/crio_M1/Vsup Voltage;0;ReadMethodType=u16M1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9503[crioConfig.End]M1_Acceleration"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Acceleration;WriteArb=1"M1_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_ERC;WriteArb=1"M1_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Fault;WriteArb=1"M1_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Status;WriteArb=1"M1_Velocity"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Velocity;WriteArb=1"M2/Phase A Currentresource=/crio_M2/Phase A Current;0;ReadMethodType=I16M2/Phase A Negresource=/crio_M2/Phase A Neg;0;WriteMethodType=boolM2/Phase A Posresource=/crio_M2/Phase A Pos;0;WriteMethodType=boolM2/Phase B Currentresource=/crio_M2/Phase B Current;0;ReadMethodType=I16M2/Phase B Negresource=/crio_M2/Phase B Neg;0;WriteMethodType=boolM2/Phase B Posresource=/crio_M2/Phase B Pos;0;WriteMethodType=boolM2/User LEDresource=/crio_M2/User LED;0;WriteMethodType=boolM2/Vsup Voltageresource=/crio_M2/Vsup Voltage;0;ReadMethodType=u16M2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9503[crioConfig.End]M2_Acceleration"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Acceleration;WriteArb=1"M2_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_ERC;WriteArb=1"M2_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Fault;WriteArb=1"M2_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Status;WriteArb=1"M2_Velocity"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Velocity;WriteArb=1"M3/Phase A Currentresource=/crio_M3/Phase A Current;0;ReadMethodType=I16M3/Phase A Negresource=/crio_M3/Phase A Neg;0;WriteMethodType=boolM3/Phase A Posresource=/crio_M3/Phase A Pos;0;WriteMethodType=boolM3/Phase B Currentresource=/crio_M3/Phase B Current;0;ReadMethodType=I16M3/Phase B Negresource=/crio_M3/Phase B Neg;0;WriteMethodType=boolM3/Phase B Posresource=/crio_M3/Phase B Pos;0;WriteMethodType=boolM3/User LEDresource=/crio_M3/User LED;0;WriteMethodType=boolM3/Vsup Voltageresource=/crio_M3/Vsup Voltage;0;ReadMethodType=u16M3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9503[crioConfig.End]M3_Acceleration"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Acceleration;WriteArb=1"M3_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_ERC;WriteArb=1"M3_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Fault;WriteArb=1"M3_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Status;WriteArb=1"M3_Velocity"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Velocity;WriteArb=1"RTD/RTD0resource=/crio_RTD/RTD0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD/RTD1resource=/crio_RTD/RTD1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD/RTD2resource=/crio_RTD/RTD2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD/RTD3resource=/crio_RTD/RTD3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9217,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.RTD_A=3,908300E-3,cRIOModule.AI0.RTD_B=-5,775000E-7,cRIOModule.AI0.RTD_C=-4,183000E-12,cRIOModule.AI0.RTD_Ro=1,000000E+2,cRIOModule.AI0.RTDType=1,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.RTD_A=3,908300E-3,cRIOModule.AI1.RTD_B=-5,775000E-7,cRIOModule.AI1.RTD_C=-4,183000E-12,cRIOModule.AI1.RTD_Ro=1,000000E+2,cRIOModule.AI1.RTDType=1,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.RTD_A=3,908300E-3,cRIOModule.AI2.RTD_B=-5,775000E-7,cRIOModule.AI2.RTD_C=-4,183000E-12,cRIOModule.AI2.RTD_Ro=1,000000E+2,cRIOModule.AI2.RTDType=1,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.RTD_A=3,908300E-3,cRIOModule.AI3.RTD_B=-5,775000E-7,cRIOModule.AI3.RTD_C=-4,183000E-12,cRIOModule.AI3.RTD_Ro=1,000000E+2,cRIOModule.AI3.RTDType=1,cRIOModule.Conversion Time=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolUSER FPGA LEDresource=/USER FPGA LED;0;ReadMethodType=u8;WriteMethodType=u8</Property>
					</Item>
				</Item>
				<Item Name="Register" Type="Folder">
					<Item Name="M1_ERC" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_ERC;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7D1AE7CD-0818-48B0-975B-F4F914EF5AB4}</Property>
						<Property Name="Initial Data" Type="Str">0000000000000000</Property>
						<Property Name="Initialized" Type="Bool">true</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M1_Acceleration" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Acceleration;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{006F05D7-61BB-407A-979B-D192B71E1E34}</Property>
						<Property Name="Initial Data" Type="Str">0000000000000000000000000000000000000000000000000000000000000000</Property>
						<Property Name="Initialized" Type="Bool">true</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M1_Velocity" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Velocity;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3D3E5081-5FBE-4379-8912-E9002F32C7B9}</Property>
						<Property Name="Initial Data" Type="Str">0000000000000000000000000000000000000000000000000000000000000000</Property>
						<Property Name="Initialized" Type="Bool">true</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M1_Fault" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Fault;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{45FD5D6A-17EA-48FB-B0AA-51845FEDA88B}</Property>
						<Property Name="Initial Data" Type="Str">0000000000000000</Property>
						<Property Name="Initialized" Type="Bool">true</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M1_Status" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Status;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{72C93886-6840-4846-AB65-9493DE3510AF}</Property>
						<Property Name="Initial Data" Type="Str">0000000000000000</Property>
						<Property Name="Initialized" Type="Bool">true</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M2_ERC" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_ERC;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E96C53ED-AA73-4E47-974B-DD37928C487D}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M2_Acceleration" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Acceleration;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{CC2385AB-B3FF-4851-92E2-8DDCA015C218}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M2_Velocity" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Velocity;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F0EF8321-578F-4BB4-B502-EC1F74FEEA29}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M2_Fault" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Fault;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{96719503-0785-4051-BC5B-E5DE7BDEE8D6}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M2_Status" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Status;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E6B79732-A2E6-48BD-9785-AF8DD5BB77E0}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M3_ERC" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_ERC;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8458B58A-7AD1-4E16-BAC5-EBE2DF32D274}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M3_Acceleration" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Acceleration;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{010E016C-E1E4-44AA-AF12-A394C6BFD24D}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M3_Velocity" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Velocity;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A1A2D640-42E7-4F71-8570-D754AAAD4E54}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M3_Fault" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Fault;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{15233ADB-26D7-4AB8-9A49-F70EC69EE51D}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M3_Status" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Status;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E0B41708-B684-4017-B93F-1D76B222FA17}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
				</Item>
				<Item Name="IO" Type="Folder">
					<Item Name="AO" Type="Folder">
						<Item Name="AO/AO0" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_AO/AO0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{67D6B436-478D-42AD-A98A-02D9F02511E0}</Property>
						</Item>
						<Item Name="AO/AO1" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_AO/AO1</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{07251ABD-BBB2-4D32-BE9F-144658305956}</Property>
						</Item>
						<Item Name="AO/AO2" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_AO/AO2</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{026FDC5C-D601-4357-9DCA-D02573CA54BC}</Property>
						</Item>
						<Item Name="AO/AO3" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_AO/AO3</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{3BD16E5F-9BC1-4A1E-AC09-2971644802DB}</Property>
						</Item>
					</Item>
					<Item Name="DIO" Type="Folder">
						<Item Name="DIO/DIO0" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{21E02E54-3F81-4804-968A-C4D015E2820D}</Property>
						</Item>
						<Item Name="DIO/DIO1" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO1</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{6E015061-A9DB-4F6A-BAB5-D7142A92B1EC}</Property>
						</Item>
						<Item Name="DIO/DIO2" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO2</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{6ADFC21D-443B-4DBA-B07E-8CC9C92D5007}</Property>
						</Item>
						<Item Name="DIO/DIO3" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{9479962B-5A8F-432A-8D41-7D25190ED3D8}</Property>
						</Item>
						<Item Name="DIO/DIO4" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO4</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{F60E166B-0FAA-4B74-A8B8-BB5499263F2D}</Property>
						</Item>
						<Item Name="DIO/DIO5" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO5</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{38D49020-B080-4144-B3E3-E4A0A4306257}</Property>
						</Item>
						<Item Name="DIO/DIO6" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{7B3B7C2C-A1BD-4B0E-AE2B-C895184A8CB1}</Property>
						</Item>
						<Item Name="DIO/DIO7" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO7</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{1901449E-2C86-49CB-9C06-17F5B0B1D0B0}</Property>
						</Item>
						<Item Name="DIO/DIO8" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{4F3F9BF8-D79D-45BD-894A-4624B836C30A}</Property>
						</Item>
						<Item Name="DIO/DIO9" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO9</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{2BDA572E-33F4-4691-AA99-80CBF303AAE1}</Property>
						</Item>
						<Item Name="DIO/DIO10" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO10</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{A3E30E0A-2957-4464-9D86-6E0D826420BA}</Property>
						</Item>
						<Item Name="DIO/DIO11" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO11</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{CF3E83A5-BCDB-4F41-A0C0-5429AD8314C6}</Property>
						</Item>
						<Item Name="DIO/DIO12" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO12</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{63B3BFC5-043E-4D3C-8292-709646D94218}</Property>
						</Item>
						<Item Name="DIO/DIO13" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO13</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{0F53BB40-C28C-45BC-B323-1DFDF3BB500A}</Property>
						</Item>
						<Item Name="DIO/DIO14" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO14</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{14DD4532-44FC-4FE7-AEE7-92449120EEA8}</Property>
						</Item>
						<Item Name="DIO/DIO15" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO15</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{4477D065-8069-4CA4-80E6-F0A79D39BF55}</Property>
						</Item>
						<Item Name="DIO/DIO16" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO16</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{82E44D0E-A81F-4A61-B400-236726FE216E}</Property>
						</Item>
						<Item Name="DIO/DIO17" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO17</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{2BF18089-F30E-49F3-B51F-A4922E402A1F}</Property>
						</Item>
						<Item Name="DIO/DIO18" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO18</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{96F5C267-2D92-4F2D-ADF6-B98333008BCD}</Property>
						</Item>
						<Item Name="DIO/DIO19" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO19</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{3C46C8E7-A00A-4453-A520-F51672F00837}</Property>
						</Item>
						<Item Name="DIO/DIO20" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO20</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{47D82495-122D-47CE-9129-3E94D4F674B7}</Property>
						</Item>
						<Item Name="DIO/DIO21" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO21</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{4F6DC4BE-4506-4BEF-8065-C5F46E77ECA9}</Property>
						</Item>
						<Item Name="DIO/DIO22" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO22</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{37AC7FE7-D68D-4BB6-8B89-B149D0321D6D}</Property>
						</Item>
						<Item Name="DIO/DIO23" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO23</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{F5D92A55-D78C-49F3-9C7F-EA1BC351B7D6}</Property>
						</Item>
						<Item Name="DIO/DIO24" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO24</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{74591987-52CC-4938-8CA3-61B3402D61D6}</Property>
						</Item>
						<Item Name="DIO/DIO25" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO25</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{48C0C5DB-0FCD-4D2A-B858-B8FA9CF450CD}</Property>
						</Item>
						<Item Name="DIO/DIO26" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO26</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{BC363C0F-8C6E-42DC-84FA-8C6E274FEFD1}</Property>
						</Item>
						<Item Name="DIO/DIO27" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO27</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{40BE5CC7-CDD4-420F-AA25-7F1DA696E32B}</Property>
						</Item>
						<Item Name="DIO/DIO28" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO28</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{D6EA9CCA-640E-4584-B32B-4986F6D93E94}</Property>
						</Item>
						<Item Name="DIO/DIO29" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO29</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{3122BB88-70C9-4ECE-87BE-FCAE9D40D781}</Property>
						</Item>
						<Item Name="DIO/DIO30" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO30</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{E1099124-0820-457D-B873-1105BA1A109F}</Property>
						</Item>
						<Item Name="DIO/DIO31" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO31</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{7E1AD0C5-A11A-4F87-859D-F33EFEE09DE8}</Property>
						</Item>
						<Item Name="DIO/DIO7:0" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{2224891D-A82A-4BCD-AEF6-8F587802C8FC}</Property>
						</Item>
						<Item Name="DIO/DIO15:8" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{C1927FA7-D3DC-4FF9-B56B-E922DD08C0B4}</Property>
						</Item>
						<Item Name="DIO/DIO23:16" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{725EBB60-DE48-4E19-B249-4727EFC6771D}</Property>
						</Item>
						<Item Name="DIO/DIO31:24" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{880D9FFC-FD5D-4A1F-99F4-93F82E1B2CD6}</Property>
						</Item>
						<Item Name="DIO/DIO31:0" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_DIO/DIO31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{13CD6CAA-198B-4FD9-AE0A-F4D520AB051E}</Property>
						</Item>
					</Item>
					<Item Name="AI" Type="Folder">
						<Item Name="AI/AI0" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_AI/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{54AFFFEC-1B0D-4C24-B6AC-D10F1418A425}</Property>
						</Item>
						<Item Name="AI/AI1" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_AI/AI1</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{85EB5E73-E7F0-475C-BC32-B2711096EAE3}</Property>
						</Item>
						<Item Name="AI/AI2" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_AI/AI2</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{E851EB27-3902-4779-8A5E-3E37CD923599}</Property>
						</Item>
						<Item Name="AI/AI3" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_AI/AI3</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{ECA728EE-42D4-4F79-B429-F266DA77039E}</Property>
						</Item>
					</Item>
					<Item Name="RTD" Type="Folder">
						<Item Name="RTD/RTD0" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_RTD/RTD0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{70366361-0FD2-486E-A521-CB6B726DA0B2}</Property>
						</Item>
						<Item Name="RTD/RTD1" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_RTD/RTD1</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{85F0BA7B-C7CC-49B0-81A1-495F09F5E23B}</Property>
						</Item>
						<Item Name="RTD/RTD2" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_RTD/RTD2</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{A58590BE-D062-4BCD-A7A3-C2C956BA19CF}</Property>
						</Item>
						<Item Name="RTD/RTD3" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_RTD/RTD3</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{2E3868E1-2357-4D16-9E98-C531AEED6315}</Property>
						</Item>
					</Item>
					<Item Name="M3" Type="Folder">
						<Item Name="M3/Phase A Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase A Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{2A83F047-5CEA-44D6-93FB-244FB6A9A93F}</Property>
						</Item>
						<Item Name="M3/Phase A Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase A Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{7BA7AB73-6BDD-4EE1-B207-ADB98264EBA9}</Property>
						</Item>
						<Item Name="M3/Phase B Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase B Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{BAB8EF2B-F6C8-4B14-98FF-6CD2AF45823E}</Property>
						</Item>
						<Item Name="M3/Phase B Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase B Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{D8F98CFB-0029-4C76-BDA7-72A9D634104D}</Property>
						</Item>
						<Item Name="M3/Phase A Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase A Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{277EBE1A-A6CC-4EDF-A83E-FCB2C8061743}</Property>
						</Item>
						<Item Name="M3/Phase B Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase B Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{51429A00-FAAF-46EF-9962-60AAD867809A}</Property>
						</Item>
						<Item Name="M3/Vsup Voltage" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Vsup Voltage</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{C351C620-4AA5-4984-9222-41FDFE5DD228}</Property>
						</Item>
						<Item Name="M3/User LED" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/User LED</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{4EC7DC1B-D472-4B38-B84D-723A1938F8B8}</Property>
						</Item>
					</Item>
					<Item Name="M2" Type="Folder">
						<Item Name="M2/Phase A Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase A Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{CE99DF16-01C5-4A2B-9DF9-F78C5E3A7A60}</Property>
						</Item>
						<Item Name="M2/Phase A Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase A Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{51B91DB3-8528-4B79-A63D-C6FB8DF2F999}</Property>
						</Item>
						<Item Name="M2/Phase B Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase B Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{49E5CD12-D57E-4B7A-9D48-85E14B4FA6D5}</Property>
						</Item>
						<Item Name="M2/Phase B Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase B Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{89784F34-D11A-4B88-9628-F1F96A36103A}</Property>
						</Item>
						<Item Name="M2/Phase A Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase A Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{6BEF824D-AA88-48B6-8919-AC5612FD709C}</Property>
						</Item>
						<Item Name="M2/Phase B Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase B Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{47F67B21-77C9-4082-B039-4D98371FBAB8}</Property>
						</Item>
						<Item Name="M2/Vsup Voltage" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Vsup Voltage</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{46106632-8333-4468-B006-E2D67DCC25E9}</Property>
						</Item>
						<Item Name="M2/User LED" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/User LED</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{8E931E92-530F-4D82-A5D5-8D08593EA76F}</Property>
						</Item>
					</Item>
					<Item Name="M1" Type="Folder">
						<Item Name="M1/Phase A Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase A Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{9DA713EE-5A9C-44B1-9400-77B8EDA367DD}</Property>
						</Item>
						<Item Name="M1/Phase A Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase A Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{3D2524F3-2686-4566-8032-1641526799AB}</Property>
						</Item>
						<Item Name="M1/Phase B Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase B Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{C6E5A006-DD94-4689-87E8-BE6797E60D0E}</Property>
						</Item>
						<Item Name="M1/Phase B Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase B Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{FA13C610-4BD7-4402-AE9C-56D700D751F4}</Property>
						</Item>
						<Item Name="M1/Phase A Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase A Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{201594B3-DA05-472F-B454-E93CB39D9CD2}</Property>
						</Item>
						<Item Name="M1/Phase B Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase B Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{D2112507-8C9A-4C91-9924-650FA96DD702}</Property>
						</Item>
						<Item Name="M1/Vsup Voltage" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Vsup Voltage</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{BF8083E2-842C-4743-87AC-39EA4178D766}</Property>
						</Item>
						<Item Name="M1/User LED" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/User LED</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{F1EC0C53-F395-4B6B-8830-D525ED9D26D0}</Property>
						</Item>
					</Item>
					<Item Name="Chassis I/O" Type="Folder">
						<Item Name="Chassis Temperature" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Chassis Temperature</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{A62615B2-7518-44A2-9FE1-8418CB3DF33C}</Property>
						</Item>
						<Item Name="Scan Clock" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Scan Clock</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{53F32106-4D81-471E-9300-07B9D924A859}</Property>
						</Item>
						<Item Name="Sleep" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Sleep</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{74B52144-4566-4772-9E96-53028B4CE236}</Property>
						</Item>
						<Item Name="System Reset" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/System Reset</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{5A5BC916-492C-4E93-890E-945655CDB66D}</Property>
						</Item>
						<Item Name="USER FPGA LED" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/USER FPGA LED</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{C08DF885-E8A2-4D7A-8B48-F46326296823}</Property>
						</Item>
					</Item>
				</Item>
				<Item Name="Main.vi" Type="VI" URL="../Main.vi">
					<Property Name="BuildSpec" Type="Str">{325B2332-05F2-4DD2-97AE-8343BE4F25B2}</Property>
					<Property Name="configString.guid" Type="Str">{006F05D7-61BB-407A-979B-D192B71E1E34}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Acceleration;WriteArb=1"{010E016C-E1E4-44AA-AF12-A394C6BFD24D}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Acceleration;WriteArb=1"{026FDC5C-D601-4357-9DCA-D02573CA54BC}resource=/crio_AO/AO2;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{07251ABD-BBB2-4D32-BE9F-144658305956}resource=/crio_AO/AO1;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{0F53BB40-C28C-45BC-B323-1DFDF3BB500A}resource=/crio_DIO/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{12C364F6-6FC3-43ED-A15E-1C831422CA44}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9503[crioConfig.End]{13CD6CAA-198B-4FD9-AE0A-F4D520AB051E}resource=/crio_DIO/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{14DD4532-44FC-4FE7-AEE7-92449120EEA8}resource=/crio_DIO/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{15233ADB-26D7-4AB8-9A49-F70EC69EE51D}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Fault;WriteArb=1"{1901449E-2C86-49CB-9C06-17F5B0B1D0B0}resource=/crio_DIO/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{201594B3-DA05-472F-B454-E93CB39D9CD2}resource=/crio_M1/Phase A Current;0;ReadMethodType=I16{21E02E54-3F81-4804-968A-C4D015E2820D}resource=/crio_DIO/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{2224891D-A82A-4BCD-AEF6-8F587802C8FC}resource=/crio_DIO/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{277EBE1A-A6CC-4EDF-A83E-FCB2C8061743}resource=/crio_M3/Phase A Current;0;ReadMethodType=I16{2A83F047-5CEA-44D6-93FB-244FB6A9A93F}resource=/crio_M3/Phase A Pos;0;WriteMethodType=bool{2BDA572E-33F4-4691-AA99-80CBF303AAE1}resource=/crio_DIO/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{2BF18089-F30E-49F3-B51F-A4922E402A1F}resource=/crio_DIO/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{2E3868E1-2357-4D16-9E98-C531AEED6315}resource=/crio_RTD/RTD3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{3122BB88-70C9-4ECE-87BE-FCAE9D40D781}resource=/crio_DIO/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{37AC7FE7-D68D-4BB6-8B89-B149D0321D6D}resource=/crio_DIO/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{38D49020-B080-4144-B3E3-E4A0A4306257}resource=/crio_DIO/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{3BD16E5F-9BC1-4A1E-AC09-2971644802DB}resource=/crio_AO/AO3;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{3C46C8E7-A00A-4453-A520-F51672F00837}resource=/crio_DIO/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{3C642664-A6F3-4C66-A1B9-0213307023E5}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9215,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{3D2524F3-2686-4566-8032-1641526799AB}resource=/crio_M1/Phase A Neg;0;WriteMethodType=bool{3D3E5081-5FBE-4379-8912-E9002F32C7B9}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Velocity;WriteArb=1"{3E530D7E-864F-43D0-AF49-15B5CDE2BB5F}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{40BE5CC7-CDD4-420F-AA25-7F1DA696E32B}resource=/crio_DIO/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{4477D065-8069-4CA4-80E6-F0A79D39BF55}resource=/crio_DIO/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{45FD5D6A-17EA-48FB-B0AA-51845FEDA88B}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Fault;WriteArb=1"{46106632-8333-4468-B006-E2D67DCC25E9}resource=/crio_M2/Vsup Voltage;0;ReadMethodType=u16{47D82495-122D-47CE-9129-3E94D4F674B7}resource=/crio_DIO/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{47F67B21-77C9-4082-B039-4D98371FBAB8}resource=/crio_M2/Phase B Current;0;ReadMethodType=I16{48C0C5DB-0FCD-4D2A-B858-B8FA9CF450CD}resource=/crio_DIO/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{49E5CD12-D57E-4B7A-9D48-85E14B4FA6D5}resource=/crio_M2/Phase B Pos;0;WriteMethodType=bool{4EC7DC1B-D472-4B38-B84D-723A1938F8B8}resource=/crio_M3/User LED;0;WriteMethodType=bool{4F3F9BF8-D79D-45BD-894A-4624B836C30A}resource=/crio_DIO/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{4F6DC4BE-4506-4BEF-8065-C5F46E77ECA9}resource=/crio_DIO/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{51429A00-FAAF-46EF-9962-60AAD867809A}resource=/crio_M3/Phase B Current;0;ReadMethodType=I16{51B91DB3-8528-4B79-A63D-C6FB8DF2F999}resource=/crio_M2/Phase A Neg;0;WriteMethodType=bool{53F32106-4D81-471E-9300-07B9D924A859}resource=/Scan Clock;0;ReadMethodType=bool{54AFFFEC-1B0D-4C24-B6AC-D10F1418A425}resource=/crio_AI/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{5A5BC916-492C-4E93-890E-945655CDB66D}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{63AE5859-9011-477E-8E4F-375473740DAF}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{63B3BFC5-043E-4D3C-8292-709646D94218}resource=/crio_DIO/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{67D6B436-478D-42AD-A98A-02D9F02511E0}resource=/crio_AO/AO0;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{6ADFC21D-443B-4DBA-B07E-8CC9C92D5007}resource=/crio_DIO/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{6BEF824D-AA88-48B6-8919-AC5612FD709C}resource=/crio_M2/Phase A Current;0;ReadMethodType=I16{6E015061-A9DB-4F6A-BAB5-D7142A92B1EC}resource=/crio_DIO/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{70366361-0FD2-486E-A521-CB6B726DA0B2}resource=/crio_RTD/RTD0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{725EBB60-DE48-4E19-B249-4727EFC6771D}resource=/crio_DIO/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{72C93886-6840-4846-AB65-9493DE3510AF}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Status;WriteArb=1"{74591987-52CC-4938-8CA3-61B3402D61D6}resource=/crio_DIO/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{74B52144-4566-4772-9E96-53028B4CE236}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{7B3B7C2C-A1BD-4B0E-AE2B-C895184A8CB1}resource=/crio_DIO/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{7BA7AB73-6BDD-4EE1-B207-ADB98264EBA9}resource=/crio_M3/Phase A Neg;0;WriteMethodType=bool{7D1AE7CD-0818-48B0-975B-F4F914EF5AB4}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_ERC;WriteArb=1"{7E1AD0C5-A11A-4F87-859D-F33EFEE09DE8}resource=/crio_DIO/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{82E44D0E-A81F-4A61-B400-236726FE216E}resource=/crio_DIO/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{8458B58A-7AD1-4E16-BAC5-EBE2DF32D274}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_ERC;WriteArb=1"{85EB5E73-E7F0-475C-BC32-B2711096EAE3}resource=/crio_AI/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{85F0BA7B-C7CC-49B0-81A1-495F09F5E23B}resource=/crio_RTD/RTD1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{880D9FFC-FD5D-4A1F-99F4-93F82E1B2CD6}resource=/crio_DIO/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{89784F34-D11A-4B88-9628-F1F96A36103A}resource=/crio_M2/Phase B Neg;0;WriteMethodType=bool{8B27681D-ED0E-4840-BBF9-195F6E85BEF9}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9269,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.HotSwapMode=0,cRIOModule.RsiAttributes=[crioConfig.End]{8E931E92-530F-4D82-A5D5-8D08593EA76F}resource=/crio_M2/User LED;0;WriteMethodType=bool{9479962B-5A8F-432A-8D41-7D25190ED3D8}resource=/crio_DIO/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{96719503-0785-4051-BC5B-E5DE7BDEE8D6}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Fault;WriteArb=1"{96F5C267-2D92-4F2D-ADF6-B98333008BCD}resource=/crio_DIO/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{98B58B44-DAD4-42D3-AFD4-3696CECDBDA5}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9503[crioConfig.End]{9DA713EE-5A9C-44B1-9400-77B8EDA367DD}resource=/crio_M1/Phase A Pos;0;WriteMethodType=bool{A1A2D640-42E7-4F71-8570-D754AAAD4E54}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Velocity;WriteArb=1"{A3E30E0A-2957-4464-9D86-6E0D826420BA}resource=/crio_DIO/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{A58590BE-D062-4BCD-A7A3-C2C956BA19CF}resource=/crio_RTD/RTD2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{A62615B2-7518-44A2-9FE1-8418CB3DF33C}resource=/Chassis Temperature;0;ReadMethodType=i16{BA0759DE-DEB9-4115-8F06-4C2D0579BA83}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9503[crioConfig.End]{BAB8EF2B-F6C8-4B14-98FF-6CD2AF45823E}resource=/crio_M3/Phase B Pos;0;WriteMethodType=bool{BC363C0F-8C6E-42DC-84FA-8C6E274FEFD1}resource=/crio_DIO/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{BF8083E2-842C-4743-87AC-39EA4178D766}resource=/crio_M1/Vsup Voltage;0;ReadMethodType=u16{C08DF885-E8A2-4D7A-8B48-F46326296823}resource=/USER FPGA LED;0;ReadMethodType=u8;WriteMethodType=u8{C1927FA7-D3DC-4FF9-B56B-E922DD08C0B4}resource=/crio_DIO/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{C351C620-4AA5-4984-9222-41FDFE5DD228}resource=/crio_M3/Vsup Voltage;0;ReadMethodType=u16{C54EF70B-9469-4FB7-978D-65971D7E30D1}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9217,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.RTD_A=3,908300E-3,cRIOModule.AI0.RTD_B=-5,775000E-7,cRIOModule.AI0.RTD_C=-4,183000E-12,cRIOModule.AI0.RTD_Ro=1,000000E+2,cRIOModule.AI0.RTDType=1,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.RTD_A=3,908300E-3,cRIOModule.AI1.RTD_B=-5,775000E-7,cRIOModule.AI1.RTD_C=-4,183000E-12,cRIOModule.AI1.RTD_Ro=1,000000E+2,cRIOModule.AI1.RTDType=1,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.RTD_A=3,908300E-3,cRIOModule.AI2.RTD_B=-5,775000E-7,cRIOModule.AI2.RTD_C=-4,183000E-12,cRIOModule.AI2.RTD_Ro=1,000000E+2,cRIOModule.AI2.RTDType=1,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.RTD_A=3,908300E-3,cRIOModule.AI3.RTD_B=-5,775000E-7,cRIOModule.AI3.RTD_C=-4,183000E-12,cRIOModule.AI3.RTD_Ro=1,000000E+2,cRIOModule.AI3.RTDType=1,cRIOModule.Conversion Time=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C6E5A006-DD94-4689-87E8-BE6797E60D0E}resource=/crio_M1/Phase B Pos;0;WriteMethodType=bool{CC2385AB-B3FF-4851-92E2-8DDCA015C218}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Acceleration;WriteArb=1"{CE99DF16-01C5-4A2B-9DF9-F78C5E3A7A60}resource=/crio_M2/Phase A Pos;0;WriteMethodType=bool{CF3E83A5-BCDB-4F41-A0C0-5429AD8314C6}resource=/crio_DIO/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{D2112507-8C9A-4C91-9924-650FA96DD702}resource=/crio_M1/Phase B Current;0;ReadMethodType=I16{D6EA9CCA-640E-4584-B32B-4986F6D93E94}resource=/crio_DIO/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{D8F98CFB-0029-4C76-BDA7-72A9D634104D}resource=/crio_M3/Phase B Neg;0;WriteMethodType=bool{E0B41708-B684-4017-B93F-1D76B222FA17}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Status;WriteArb=1"{E1099124-0820-457D-B873-1105BA1A109F}resource=/crio_DIO/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{E6B79732-A2E6-48BD-9785-AF8DD5BB77E0}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Status;WriteArb=1"{E851EB27-3902-4779-8A5E-3E37CD923599}resource=/crio_AI/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{E96C53ED-AA73-4E47-974B-DD37928C487D}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_ERC;WriteArb=1"{ECA728EE-42D4-4F79-B429-F266DA77039E}resource=/crio_AI/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{F0EF8321-578F-4BB4-B502-EC1F74FEEA29}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Velocity;WriteArb=1"{F1EC0C53-F395-4B6B-8830-D525ED9D26D0}resource=/crio_M1/User LED;0;WriteMethodType=bool{F5D92A55-D78C-49F3-9C7F-EA1BC351B7D6}resource=/crio_DIO/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{F60E166B-0FAA-4B74-A8B8-BB5499263F2D}resource=/crio_DIO/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{FA13C610-4BD7-4402-AE9C-56D700D751F4}resource=/crio_M1/Phase B Neg;0;WriteMethodType=boolcRIO-9068/Motor,1;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9068FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
					<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EAI/AI0resource=/crio_AI/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI/AI1resource=/crio_AI/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI/AI2resource=/crio_AI/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI/AI3resource=/crio_AI/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAI[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9215,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]AO/AO0resource=/crio_AO/AO0;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO/AO1resource=/crio_AO/AO1;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO/AO2resource=/crio_AO/AO2;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO/AO3resource=/crio_AO/AO3;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlAO[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9269,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.HotSwapMode=0,cRIOModule.RsiAttributes=[crioConfig.End]Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9068/Motor,1;/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9068FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]DIO/DIO0resource=/crio_DIO/DIO0;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO10resource=/crio_DIO/DIO10;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO11resource=/crio_DIO/DIO11;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO12resource=/crio_DIO/DIO12;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO13resource=/crio_DIO/DIO13;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO14resource=/crio_DIO/DIO14;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO15:8resource=/crio_DIO/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO15resource=/crio_DIO/DIO15;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO16resource=/crio_DIO/DIO16;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO17resource=/crio_DIO/DIO17;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO18resource=/crio_DIO/DIO18;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO19resource=/crio_DIO/DIO19;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO1resource=/crio_DIO/DIO1;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO20resource=/crio_DIO/DIO20;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO21resource=/crio_DIO/DIO21;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO22resource=/crio_DIO/DIO22;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO23:16resource=/crio_DIO/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO23resource=/crio_DIO/DIO23;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO24resource=/crio_DIO/DIO24;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO25resource=/crio_DIO/DIO25;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO26resource=/crio_DIO/DIO26;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO27resource=/crio_DIO/DIO27;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO28resource=/crio_DIO/DIO28;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO29resource=/crio_DIO/DIO29;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO2resource=/crio_DIO/DIO2;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO30resource=/crio_DIO/DIO30;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO31:0resource=/crio_DIO/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32DIO/DIO31:24resource=/crio_DIO/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO31resource=/crio_DIO/DIO31;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO3resource=/crio_DIO/DIO3;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO4resource=/crio_DIO/DIO4;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO5resource=/crio_DIO/DIO5;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO6resource=/crio_DIO/DIO6;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO7:0resource=/crio_DIO/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8DIO/DIO7resource=/crio_DIO/DIO7;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO8resource=/crio_DIO/DIO8;0;ReadMethodType=bool;WriteMethodType=boolDIO/DIO9resource=/crio_DIO/DIO9;0;ReadMethodType=bool;WriteMethodType=boolDIO[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]M1/Phase A Currentresource=/crio_M1/Phase A Current;0;ReadMethodType=I16M1/Phase A Negresource=/crio_M1/Phase A Neg;0;WriteMethodType=boolM1/Phase A Posresource=/crio_M1/Phase A Pos;0;WriteMethodType=boolM1/Phase B Currentresource=/crio_M1/Phase B Current;0;ReadMethodType=I16M1/Phase B Negresource=/crio_M1/Phase B Neg;0;WriteMethodType=boolM1/Phase B Posresource=/crio_M1/Phase B Pos;0;WriteMethodType=boolM1/User LEDresource=/crio_M1/User LED;0;WriteMethodType=boolM1/Vsup Voltageresource=/crio_M1/Vsup Voltage;0;ReadMethodType=u16M1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9503[crioConfig.End]M1_Acceleration"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Acceleration;WriteArb=1"M1_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_ERC;WriteArb=1"M1_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Fault;WriteArb=1"M1_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Status;WriteArb=1"M1_Velocity"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Velocity;WriteArb=1"M2/Phase A Currentresource=/crio_M2/Phase A Current;0;ReadMethodType=I16M2/Phase A Negresource=/crio_M2/Phase A Neg;0;WriteMethodType=boolM2/Phase A Posresource=/crio_M2/Phase A Pos;0;WriteMethodType=boolM2/Phase B Currentresource=/crio_M2/Phase B Current;0;ReadMethodType=I16M2/Phase B Negresource=/crio_M2/Phase B Neg;0;WriteMethodType=boolM2/Phase B Posresource=/crio_M2/Phase B Pos;0;WriteMethodType=boolM2/User LEDresource=/crio_M2/User LED;0;WriteMethodType=boolM2/Vsup Voltageresource=/crio_M2/Vsup Voltage;0;ReadMethodType=u16M2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9503[crioConfig.End]M2_Acceleration"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Acceleration;WriteArb=1"M2_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_ERC;WriteArb=1"M2_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Fault;WriteArb=1"M2_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Status;WriteArb=1"M2_Velocity"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Velocity;WriteArb=1"M3/Phase A Currentresource=/crio_M3/Phase A Current;0;ReadMethodType=I16M3/Phase A Negresource=/crio_M3/Phase A Neg;0;WriteMethodType=boolM3/Phase A Posresource=/crio_M3/Phase A Pos;0;WriteMethodType=boolM3/Phase B Currentresource=/crio_M3/Phase B Current;0;ReadMethodType=I16M3/Phase B Negresource=/crio_M3/Phase B Neg;0;WriteMethodType=boolM3/Phase B Posresource=/crio_M3/Phase B Pos;0;WriteMethodType=boolM3/User LEDresource=/crio_M3/User LED;0;WriteMethodType=boolM3/Vsup Voltageresource=/crio_M3/Vsup Voltage;0;ReadMethodType=u16M3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9503[crioConfig.End]M3_Acceleration"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Acceleration;WriteArb=1"M3_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_ERC;WriteArb=1"M3_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Fault;WriteArb=1"M3_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Status;WriteArb=1"M3_Velocity"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Velocity;WriteArb=1"RTD/RTD0resource=/crio_RTD/RTD0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD/RTD1resource=/crio_RTD/RTD1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD/RTD2resource=/crio_RTD/RTD2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD/RTD3resource=/crio_RTD/RTD3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlRTD[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9217,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.RTD_A=3,908300E-3,cRIOModule.AI0.RTD_B=-5,775000E-7,cRIOModule.AI0.RTD_C=-4,183000E-12,cRIOModule.AI0.RTD_Ro=1,000000E+2,cRIOModule.AI0.RTDType=1,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.RTD_A=3,908300E-3,cRIOModule.AI1.RTD_B=-5,775000E-7,cRIOModule.AI1.RTD_C=-4,183000E-12,cRIOModule.AI1.RTD_Ro=1,000000E+2,cRIOModule.AI1.RTDType=1,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.RTD_A=3,908300E-3,cRIOModule.AI2.RTD_B=-5,775000E-7,cRIOModule.AI2.RTD_C=-4,183000E-12,cRIOModule.AI2.RTD_Ro=1,000000E+2,cRIOModule.AI2.RTDType=1,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.RTD_A=3,908300E-3,cRIOModule.AI3.RTD_B=-5,775000E-7,cRIOModule.AI3.RTD_C=-4,183000E-12,cRIOModule.AI3.RTD_Ro=1,000000E+2,cRIOModule.AI3.RTDType=1,cRIOModule.Conversion Time=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolUSER FPGA LEDresource=/USER FPGA LED;0;ReadMethodType=u8;WriteMethodType=u8</Property>
					<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\User\Krebs\LVP\Detlab\GDP-NI9503-Stepper-Drive\FPGA Bitfiles\GDP-NI9503-Stepp_FPGATarget_Main_DoDqZffLYJo.lvbitx</Property>
				</Item>
				<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
					<Property Name="FPGA.PersistentID" Type="Str">{63AE5859-9011-477E-8E4F-375473740DAF}</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
					<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
				</Item>
				<Item Name="IP Builder" Type="IP Builder Target">
					<Item Name="Dependencies" Type="Dependencies"/>
					<Item Name="Build Specifications" Type="Build"/>
				</Item>
				<Item Name="AO" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 1</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9269</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.HotSwapMode" Type="Str">0</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8B27681D-ED0E-4840-BBF9-195F6E85BEF9}</Property>
				</Item>
				<Item Name="DIO" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 2</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9403</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.Initial Line Direction" Type="Str">00000000000000000000000000000000</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3E530D7E-864F-43D0-AF49-15B5CDE2BB5F}</Property>
				</Item>
				<Item Name="AI" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 3</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9215</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3C642664-A6F3-4C66-A1B9-0213307023E5}</Property>
				</Item>
				<Item Name="RTD" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 4</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9217</Property>
					<Property Name="cRIOModule.AI0.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI0.RTD_A" Type="Str">3,908300E-3</Property>
					<Property Name="cRIOModule.AI0.RTD_B" Type="Str">-5,775000E-7</Property>
					<Property Name="cRIOModule.AI0.RTD_C" Type="Str">-4,183000E-12</Property>
					<Property Name="cRIOModule.AI0.RTD_Ro" Type="Str">1,000000E+2</Property>
					<Property Name="cRIOModule.AI0.RTDType" Type="Str">1</Property>
					<Property Name="cRIOModule.AI1.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI1.RTD_A" Type="Str">3,908300E-3</Property>
					<Property Name="cRIOModule.AI1.RTD_B" Type="Str">-5,775000E-7</Property>
					<Property Name="cRIOModule.AI1.RTD_C" Type="Str">-4,183000E-12</Property>
					<Property Name="cRIOModule.AI1.RTD_Ro" Type="Str">1,000000E+2</Property>
					<Property Name="cRIOModule.AI1.RTDType" Type="Str">1</Property>
					<Property Name="cRIOModule.AI2.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI2.RTD_A" Type="Str">3,908300E-3</Property>
					<Property Name="cRIOModule.AI2.RTD_B" Type="Str">-5,775000E-7</Property>
					<Property Name="cRIOModule.AI2.RTD_C" Type="Str">-4,183000E-12</Property>
					<Property Name="cRIOModule.AI2.RTD_Ro" Type="Str">1,000000E+2</Property>
					<Property Name="cRIOModule.AI2.RTDType" Type="Str">1</Property>
					<Property Name="cRIOModule.AI3.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI3.RTD_A" Type="Str">3,908300E-3</Property>
					<Property Name="cRIOModule.AI3.RTD_B" Type="Str">-5,775000E-7</Property>
					<Property Name="cRIOModule.AI3.RTD_C" Type="Str">-4,183000E-12</Property>
					<Property Name="cRIOModule.AI3.RTD_Ro" Type="Str">1,000000E+2</Property>
					<Property Name="cRIOModule.AI3.RTDType" Type="Str">1</Property>
					<Property Name="cRIOModule.Conversion Time" Type="Str">0</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C54EF70B-9469-4FB7-978D-65971D7E30D1}</Property>
				</Item>
				<Item Name="M3" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 6</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9503</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{98B58B44-DAD4-42D3-AFD4-3696CECDBDA5}</Property>
				</Item>
				<Item Name="M2" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 7</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9503</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{12C364F6-6FC3-43ED-A15E-1C831422CA44}</Property>
				</Item>
				<Item Name="M1" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 8</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9503</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BA0759DE-DEB9-4115-8F06-4C2D0579BA83}</Property>
				</Item>
				<Item Name="GDP-9503.lvlib" Type="Library" URL="../GDP-9503.lvlib"/>
				<Item Name="Dependencies" Type="Dependencies">
					<Item Name="vi.lib" Type="Folder">
						<Item Name="NI_SoftMotion_MotorControlIP.lvlib" Type="Library" URL="/&lt;vilib&gt;/Motion/MotorControl/NI_SoftMotion_MotorControlIP.lvlib"/>
						<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
						<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
					</Item>
				</Item>
				<Item Name="Build Specifications" Type="Build">
					<Item Name="Main" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">Main</Property>
						<Property Name="Comp.BitfileName" Type="Str">GDP-NI9503-Stepp_FPGATarget_Main_DoDqZffLYJo.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">9</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.OptDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.PhysOptDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.PlaceDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.RouteDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/User/Krebs/LVP/Detlab/GDP-NI9503-Stepper-Drive/FPGA Bitfiles/GDP-NI9503-Stepp_FPGATarget_Main_DoDqZffLYJo.lvbitx</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/GDP-NI9503-Stepp_FPGATarget_Main_DoDqZffLYJo.lvbitx</Property>
						<Property Name="ProjectPath" Type="Path">/C/User/Krebs/LVP/Detlab/GDP-NI9503-new-attempt/GDP-NI9503-Stepper-Drive.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">true</Property>
						<Property Name="RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
						<Property Name="TargetName" Type="Str">FPGA Target</Property>
						<Property Name="TopLevelVI" Type="Ref">/RT CompactRIO Target/Chassis/FPGA Target/Main.vi</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Real-Time Scan Resources" Type="Module Container">
				<Property Name="crio.ModuleContainerType" Type="Str">crio.RSIModuleContainer</Property>
			</Item>
		</Item>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
